# -*- coding: utf-8 -*-

'''
Created on 5 f??vr. 2012

@author: Nicolas P

Based on a perl script written by GR

'''

import httplib, urllib


def get_webdav_info(host, login, password):
    id  = postNasLogin(host, login, password);
    url = postGetNas(host, id);
    login, password = postGetCredentials(host, id);
    return url, login, password


def postNasLogin(host, login, password):
    # 'POST /cloudnas/r0/ws.dispatcher/nasLogin HTTP/1.1'."\r\n".
    # 'Content-Type: application/x-www-form-urlencoded'."\r\n".
    # 'User-Agent: hubiC/1.0.9 (Windows NT 6.1; fr_FR)'."\r\n".
    # 'Content-Length: 126'."\r\n".
    # 'Connection: Keep-Alive'."\r\n".
    # 'Accept-Encoding: gzip'."\r\n".
    # 'Accept-Language: fr-FR,en,*'."\r\n".
    # 'Host: ws.ovh.com'."\r\n".
    # ''."\r\n".
    # 'session=&params=%7B%20%22email%22%20%3A%20%22<login>%22%2C%20%22password%22%20%3A%20%22<password>%22%20%7D'."\r\n".
    # "\r\n";
    params = 'session=&params={"email":"%s","password":"%s"}' % (urllib.quote(login), urllib.quote(password))
    #print params
    headers = {"Content-type": 'application/x-www-form-urlencoded'}
    conn = httplib.HTTPSConnection(host)
    #conn.set_debuglevel(5)
    conn.request("POST", "/cloudnas/r0/ws.dispatcher/nasLogin", params, headers)
    response = conn.getresponse()
    s = response.status
    r = response.reason
    data = response.read()
    #print s, r
    #print data
    if r == 'OK' :
        null = None # null appears inside data
        d = eval(data)
        return d['answer']['id']
    else :
        print data
    conn.close()
    return None


def postGetNas(host, id):
    # 'POST /cloudnas/r0/ws.dispatcher/getNas HTTP/1.1'."\r\n".
    # 'Content-Type: application/x-www-form-urlencoded'."\r\n".
    # 'User-Agent: hubiC/1.0.9 (Windows NT 6.1; fr_FR)'."\r\n".
    # 'Content-Length: 54'."\r\n".
    # 'Connection: Keep-Alive'."\r\n".
    # 'Accept-Encoding:gzip'."\r\n".
    # 'Accept-Language: fr-FR,en,*'."\r\n".
    # 'Host: ws.ovh.com'."\r\n".
    # ''."\r\n".
    # 'session=<id>'."\r\n".
    # "\r\n";
    params = 'session=%s' % id
    headers = {"Content-type": 'application/x-www-form-urlencoded'}
    conn = httplib.HTTPSConnection(host)
    #conn.set_debuglevel(5)
    conn.request("POST", "/cloudnas/r0/ws.dispatcher/getNas", params, headers)
    response = conn.getresponse()
    s = response.status
    r = response.reason
    data = response.read()
    #print s, r
    #print data
    if r == 'OK' :
        null = None # null appears inside data
        d = eval(data)
        return d['answer']['url']
    else :
        print data
    conn.close()
    return None


def postGetCredentials(host, id):
    # 'POST /cloudnas/r0/ws.dispatcher/getCredentials HTTP/1.1'."\r\n".
    # 'Content-Type: application/x-www-form-urlencoded'."\r\n".
    # 'User-Agent: hubiC/1.0.9 (Windows NT 6.1; fr_FR)'."\r\n".
    # 'Content-Length: 54'."\r\n".
    # 'Connection: Keep-Alive'."\r\n".
    # 'Accept-Encoding: gzip'."\r\n".
    # 'Accept-Language: fr-FR,en,*'."\r\n".
    # 'Host: ws.ovh.com'."\r\n".
    # ''."\r\n".
    # 'session=<id>'."\r\n".
    # "\r\n";
    params = 'session=%s' % id
    headers = {"Content-type": 'application/x-www-form-urlencoded'}
    conn = httplib.HTTPSConnection(host)
    #conn.set_debuglevel(5)
    conn.request("POST", "/cloudnas/r0/ws.dispatcher/getCredentials", params, headers)
    response = conn.getresponse()
    s = response.status
    r = response.reason
    data = response.read()
    #print s, r
    #print data
    if r == 'OK' :
        null = None # null appears inside data
        d = eval(data)
        return d['answer']['username'], d['answer']['secret']
    else :
        print data
    conn.close()
    return None

    
if __name__ == '__main__':
    import sys
    import getpass
    
    host = "ws.ovh.com"
    try :
        login = sys.argv[1]
    except :
        login = raw_input("Login : ")
    password = getpass.getpass()

    hubic_url, hubic_login, hubic_password = get_webdav_info(host, login, password)
    
    print "URL      :", hubic_url
    print "Login    :", hubic_login
    print "Password :", hubic_password
    print "mount -t davfs %s /mnt" % hubic_url
    
    raw_input("Hit Enter to quit")
    
