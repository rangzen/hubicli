#!/bin/bash

ME=$(whoami)
if [ "x$ME" != "xroot" ]; then
	echo "This script must be run by root."
	exit
fi
apt-get install -y libneon27-dev
apt-get install -y libneon27-gnutls-dev
apt-get install -y libssl-dev
apt-get install -y libfuse-dev
apt-get install -y libglib2.0-dev
apt-get install -y fusermount
wget http://noedler.de/projekte/wdfs/wdfs-1.4.2.tar.gz
tar -xvzf wdfs-1.4.2.tar.gz
cd wdfs-1.4.2
./configure
make && make install
cd ..
rm -r wdfs-1.4.2
rm -f wdfs-1.4.2.tar.gz

