Access hubiC (OVH) from *niX
============================

[hubiC](http://www.ovh.fr/hubiC/) is a cloud space by [OVH](http://ovh.fr)
and, actually (03/2012), doesn't have *nix client.

[GR](http://www.protocol-hacking.org/) found a
[way through](http://www.protocol-hacking.org/post/2012/01/29/Hubic%2C-maintenant-vraiment-ubiquitous)
and made a perl script, Nicolas P adapts it in Python and
[cosmobob](http://forum.ubuntu-fr.org/profile.php?id=102611)
made a [shell script](http://forum.ubuntu-fr.org/viewtopic.php?pid=8211801)
to use it in userspace.

***Thanks to them!***

The code here is only an adaptation for my personal use (config file).
